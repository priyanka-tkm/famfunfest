<br/>
<div class="pure-g">
	<div class="pure-u-1 pure-u-sm-1-4">&nbsp;</div>
	<div class="pure-u-1 pure-u-sm-1-2 goodies side-gutter">
        <ul>
            <li>Hampers eksklusif dari Zwitsal</li>
            <li>Hampers eksklusif dari Sleek</li>
            <li>Satu paket sembako dari Honsetbee</li>
            <li>Hampers menarik dari Wyeth</li>
            <li>Stroller dari Mitu</li>
            <li>Hampers menarik dari Lifebuoy</li>
            <li>Essential Oil dari Belli to Baby</li>
            <li>Playmat dari Mamacoco Playmat</li>
            <li>Mop dari Claris</li>
            <li>Paket Absolut dari Evergreen & Absolut</li>
            <li>Bantal dan Mug dari Baby Clean Up</li>
            <li>Baju bayi dari Oleeva Baby&Kids Wear</li>
            <li>Hampers menarik dari Anak Sehat</li>
            <li>Produk penampung ASI dari Vita Flow</li>
        </ul>
        <p class="align-center"><strong>Dan masih banyak lagi!</strong></p>
	</div>
</div>
<br/>