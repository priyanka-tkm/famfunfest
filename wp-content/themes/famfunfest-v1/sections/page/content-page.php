<div class="sec-block page-article">
	<div class="cnt-wrap">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<article id="page-<?php echo the_ID(); ?>">
			<?php
				the_content();
			?>
		</article>
	</div>
</div>