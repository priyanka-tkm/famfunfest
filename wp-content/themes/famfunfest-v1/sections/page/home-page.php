<?php
global $post;
if ($post->post_type == 'page') {
	get_template_part( 'sections/page/content', 'page' );
} else {
	get_template_part('sections/hero-banner');
	get_template_part('sections/home-headline');
	// get_template_part('sections/home-events');
	get_template_part('sections/what-to-expect');
	get_template_part('sections/highlight-featuring');
	get_template_part('sections/sponsors');
	get_template_part('sections/sub-footer');	
}
?>