<div class="sec-block sec-what-to-expect" id="what-to-expect">
    <div class="cnt-wrap side-gutter">
        <h2 class="sec-heading the-heading">
            APA SAJA SIH, ACARANYA?
        </h2>
        <p>
            Bergabunglah bersama kami, untuk merasakan keseruan bermain bersama bayi.
        </p>
        <div class="pure-g list-what-conf">
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image1.jpg"></div>
                <!--<div class="feature-heading"><strong>Dapatkan banyak ilmu dari para pakar </strong></div>-->
                <br />
            </div>
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image2.jpg"></div>
                <!--<div class="feature-heading"><strong>Ajang berkenalan dengan orangtua lain</strong> </div>-->
                <br />
            </div>
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image3.jpg"></div>
                <!--<div class="feature-heading"><strong>Pulang dengan banyak hadiah</strong> </div>-->
                <br />
            </div>
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image6.jpg"></div>                
                <br />
            </div>
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image4.jpg"></div>
                <br />
            </div>
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <div class="feature-img"><img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/2018/feature_image5.jpg"></div>                
                <br />
            </div>
        </div>
    </div>
</div>