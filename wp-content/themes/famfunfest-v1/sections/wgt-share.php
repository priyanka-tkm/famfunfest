<div class="wgt-page-share">
    <h3 class="txt-yellow">Share this article</h3>
    <a target="_blank" href="http://www.facebook.com/share.php?u=<?=the_permalink()?>&title=<?=urlencode(the_title())?>"><i class="icon-sp-facebook"></i></a>
    &nbsp;
    <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?=urlencode(the_permalink())?>&title=<?=urlencode(the_title())?>"><i class="icon-sp-linkedin"></i></a>
    &nbsp;
    <a target="_blank" href="https://plus.google.com/share?url=<?=urlencode(the_permalink())?>"><i class="icon-sp-google-plus"></i></a>
    &nbsp;
    <a target="_blank" href="http://twitter.com/intent/tweet?status=<?=urlencode(the_title())?>%20<?=urlencode(the_permalink())?>"><i class="icon-sp-twitter"></i></a>
</div>