<div class="sec-block highlights-featuring side-gutter">
    <div class="cnt-wrap">
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-3">
                <h2 class="sec-heading the-heading">
                    HIGHLIGHTS:
                </h2>
                <ul>
                    <li>Talkshow 'Gali Potensi Anak' bersama theAsianparent Indonesia dan Klik Dokter</li>
                    <li>Tips mendampingi anak bereskplorasi bersama <a href="https://www.instagram.com/sleekbaby_id/" title="Sleek Baby">Sleek Baby</a></li>
                    <li>Talkshow 'Tetap bersih saat bereksplorasi' bersama <a href="https://www.instagram.com/mitubabyid/" title="Mitu Baby">Mitu Baby</a></li>
                    <li>Games dampingi si kecil berkreasi bersama <a href="https://www.instagram.com/honestbeeid/" title="Honestbee">Honestbee</a></li>
                    <li>Talkshow 'Pentingnya anak main di luar rumah' bersama <a href="https://www.instagram.com/zwitsal_id/" title="Zwitsal">Zwitsal</a></li>
                    <li><a href="https://id.theasianparent.com/party-planner-kidklots/" title="Dessert Table">Dessert Table</a> yang lucu dan unik</li>
                    <li><a href="https://id.theasianparent.com/allegra-photobooth-tempat-berfoto-di-pesta/" title="Photobooth">Photobooth</a> untuk mengabadikan kenangan bersama si kecil</li>
                </ul>
                <br />
            </div>

            <div class="pure-u-1 pure-u-md-2-3 l-box video-block">
                <div class="img is-video" style="background-image:url('/wp-content/themes/babybash-v1/assets/images/babybash.png')">
                    <img src="<?php echo site_url();?>/wp-content/themes/babybash-v1/assets/images/glass-article.gif">
                    <iframe class="videoplayer" data-video-url="https://www.youtube.com/embed/9cDh26u5mKE?autoplay=1" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div> 
    </div>
</div>