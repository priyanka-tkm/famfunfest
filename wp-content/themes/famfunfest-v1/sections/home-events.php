<br />
<div class="sec-block sec-home-events side-gutter">
    <div class="cnt-wrap">
        <div class="pure-g list-what-conf">
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-2">
                <span class="the-heading">DETAIL ACARA</span>
                <p>
                Tanggal: 21 Mei 2017
                <br />Waktu : Pukul 09.30 WIB - 14.00 WIB
                <br />Tempat: Hotel Century Park,<br /> Jl. Pintu Senayan, RT.1/RW.3 Gelora, <br/> Tanah Abang,  Kota Jakarta Pusat, 
                <br/>DKI Jakarta, 10270
                </p>
            </div>
            <!-- <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                <span class="the-heading">Attire</span>
                <p>Come dressed in Red and Blue, your babies too!</p>
            </div> -->
            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-2">
                <ul class="the-links">
                    <li><a href="/programme/" class="the-link">Lihat Program</a></li>
                    <li><a href="/programme/#luckydraw" class="the-link">Lucky Draw</a></li>
                    <li><a href="/programme/#goodiebag" class="the-link">Goodie Bag</a></li>
                    <li><a href="/map/" class="the-link">Menuju lokasi</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>