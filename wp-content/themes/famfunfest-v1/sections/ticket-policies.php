<div class="sec-block bg-gray-light page-article">
    <div class="cnt-wrap">
        <h1 class="sec-heading">Ticket Policies</h1>
        <h2>Refunds</h2>
        <p>
            We understand that plans change and if you need to cancel your attendance. We will refund 100% of the cost BUT only if the cancellation is done by 15th June 2017.
        </p>
        <h2>Group Discounts</h2>
        <p>
            If you’re purchasing more than 5 tickets, please e-mail us here at <a href="mailto:babybash@tickledmedia.com"><strong>thebabybash@tickledmedia.com</strong></a> for a discounted price! 
        </p>
        <h2>Transferring Tickets</h2>
        <p>
            There is no issue with transferring your ticket to another person. Just drop us an email at thebabybash@tickledmedia.com with the subject “Transfer Ticket Holder” and we’ll follow up from there.
        </p>
    </div>
</div>