<div class="sec-block bg-pink-mid sec-organizers" id="organizers">
    <div class="cnt-wrap align-center">
        <h2 class="sec-heading align-center txt-yellow">
            <small class="txt-normal">A little about the</small>
            ORGANIZERS
        </h2>
        <p>
            <img src="<?=get_stylesheet_directory_uri()?>/assets/images/logo-tm.png" alt="Tickled Media"/>
            &nbsp;
            &nbsp;
            &nbsp;
            <img src="<?=get_stylesheet_directory_uri()?>/assets/images/logo-tap-vertical.png" alt="theAsianparent"/>
        </p>

        <h3 class="txt-normal"><a href="http://tickledmedia.com" target="_blank"><span class="txt-yellow">Tickled Media</span></a> is a content &amp; community platform for parents in Asia</h3>
        <p>
            We are the people behind <a href="http://sg.theasianparent.com" target="_blank"><strong>theAsianparent.com</strong></a>, <a href="http://theindusparent.com" target="_blank"><strong>theIndusparent.com</strong></a> and <a href="https://parenttown.com" target="_blank"><strong>ParentTown</strong></a>, reaching over 7 million mums every month with content is available in 13 Asian languages.
        </p>
        <p>Tickled Media’s brand speaks to every stage of Mum’s journey – from the time she’s trying to conceive to when she’s planning a baby’s full month celebration through to when her children are off to primary school. We want to help parents raise happy, healthy and confident children
        </p>
    </div>
</div>