<?php 
require_once (ABSPATH . 'utils/ImageUtil.php');
require_once (ABSPATH . 'utils/StringUtil.php');
$imageUtil = new ImageUtil();
$stringUtil =  new StringUtil();
			$args = [
	        	'numberposts' =>-1,
	        	'post_type' => 'brand'
	      	];
      		$brand = get_posts($args);
      	?>
      	<?php if(count($brand) > 0) { ?>
			<div class="brand">
				<div class="pure-g">
					<div class="pure-u-1 pure-u-md-1 amazing-prize">
						<h2 class="sec-heading">Amazing prizes from</h2>
					</div>
				<?php foreach ($brand as $row) : 
							//$metadetails = getAllPostMeta($row->ID);
							$thumbnail   = has_post_thumbnail($row->ID) ? wp_get_attachment_image_src( get_post_thumbnail_id($row->ID), 'large') : false;
							$brandImage 	= '';
							if (!empty($thumbnail[0])) {
								$brandImage 	= $imageUtil->getThumb($thumbnail[0], 244, 96, false);
							}
							$brandDesc  	= $stringUtil->cleanContent($row->post_content);
				?>
				<div class="pure-u-1-2 pure-u-md-1-3 align-center">
					<?php if (!empty($brandImage))  : ?>
					<img src="<?php echo $brandImage; ?>">
					<?php endif;?>
					<div class="sub-title">
					    <div class="t-title"><?php echo $row->post_name; ?></div>
					    <div class="t-subtitle">
							<?php echo $brandDesc; ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			</div>
			<?php } ?>