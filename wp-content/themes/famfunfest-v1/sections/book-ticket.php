<div class="sec-block sec-contactus side-gutter" id="contactus">
    <div class="cnt-wrap" style="max-width: 700px;">
    <br />
        <div class="tickets-header pure-g">
            <div class="pure-u-1">
                <div class="pure-g prices">
                    <!--<div class="pure-u-1 pure-u-sm-1-2 l-box">
                        <h4 class="txt-pink-dark">HTM Early Bird</h4>
                        <h2 class="txt-pink-dark">Rp.250.000</h2>
                        <span class="blabla">
                            <span class="txt-pink-dark">tiket</span><br/>
                            <span>21 November - 05 Desember 2017</span>
                        </span>
                    </div>
                    <div class="pure-u-1 pure-u-sm-1-2 r-box">
                        <h4 class="txt-pink txt-normal">HTM Reguler</h4>
                        <h2 class="txt-pink">Rp.300.000</h2>
                        <span class="blabla">
                            <span class="txt-pink">tiket</span><br/>
                            <span>06-09 Desember 2017</span>
                        </span>
                    </div>-->
                    <div class="pure-u-1 pure-u-sm-1-1 l-box single-price">
                        <h2 class="txt-pink">Rp 300,000,-/</h2><span class="txt-pink">tiket (untuk bayi, ayah, bunda)</span>
                    </div>
                </div>
                <!-- <p class="ticket-txt">Tiket berlaku untuk 1 keluarga (1 bayi dan 2 orang dewasa)</p> -->
            </div>

        </div>

        <div class="form-embedded-link">
            <p>
                Yuk daftar via EventEvent mobile app yang ada di Google Playstore dan Apple Store: <a href="https://eventevent.app.link/LwhixrJenM">https://eventevent.app.link/LwhixrJenM</a>
            </p>
            <p>
                Parents, ayo segera daftarkan diri Anda di event playdate paling seru, Baby Bash! 
            </p>
            <p>
                Caranya mudah, Parents cukup download dan install aplikasi EventEvent yang ada di Google Playstore maupun Apple Store.  Silakan ketik dan cari Baby Bash Mei 2018 pada aplikasi EventEvent.
            </p>
        </div>
        <!-- <div class="ticket-frame">
            <iframe id="ticket-frame" src="https://docs.google.com/forms/d/e/1FAIpQLSeYH7aWxjJMpcItDJdpj-KHx41JROmh_18Grxa9ruJazMCehQ/viewform?embedded=true" frameborder="0" width="100%"  marginheight="0" marginwidth="0" scrolling="auto" allowtransparency="true"></iframe>
        </div> -->
    </div>
</div>
<script type="text/javascript">
    window.onload = function () {
        setIframeHeight(document.getElementById('ticket-frame'));
    };
        function setIframeHeight(iframe) {
            if (iframe) {
                var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    iframe.height = iframeWin.document.documentElement.scrollHeight || iframeWin.document.body.scrollHeight;
                }
            }
        };
    </script>
<?php //get_template_part('sections/ticket-policies'); ?>
