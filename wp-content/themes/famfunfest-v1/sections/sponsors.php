<?php
$sponserList = [];
if (class_exists('CustomSponsers')) {
    $customSponser = new CustomSponsers();
    $sponserList   = $customSponser->getSponsers();
}
$sponsers = $mediaPartners= []; 
foreach ($sponserList as $sponser) {
    if ($sponser['type'] == 'media-partnerships') {
        $mediaPartners[] = $sponser;
    } else {
        $sponsers[] = $sponser;
    }
}
$sponserCount = count($sponsers);
$mediaPartnersCount = count($mediaPartners);
if ($sponserCount > 0 || $mediaPartnersCount > 0) {
?>
<div class="sec-block sec-sponsors" id="sponsors">
    <br />
    <div class="cnt-wrap align-center">
        <?php if ($sponserCount > 0) { ?>
        <h2 class="sec-heading the-heading">
            Jadi Sponsor
        </h2>
            <ul class="pure-g list-sponsors">
            <?php foreach ($sponsers as $sponser) { ?>
                <li class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-4">
                    <?php if (!empty($sponser['url'])) { ?>
                        <a href="<?php echo $sponser['url'];?>" target="_blank" <?php if ($sponser['title']) { ?> title="<?php echo $sponser['title'];?>" <?php } ?>>
                            <img alt="<?php echo $sponser['title'];?>" src="<?php echo $sponser['image'];?>">
                        </a>
                    <?php } else { ?>
                        <img <?php if ($sponser['title']) { ?> alt="<?php echo $sponser['title'];?>" <?php } ?> src="<?php echo $sponser['image'];?>">
                    <?php } ?>
                </li>
            <?php } ?>
            </ul>
        <?php } ?>
        <?php if ($mediaPartnersCount > 0) { ?>
        <h2 class="sec-heading">
            <?php if ($sponserCount == 0) { ?>
                <small>With some help from our</small>
            <?php } ?>
            MEDIA PARTNERS
        </h2>
            <ul class="pure-g list-sponsors">
            <?php foreach ($mediaPartners as $mediaPartner) { ?>
                <li class="pure-u-1 pure-u-sm-1-2 pure-u-md-1-3">
                    <?php if (!empty($mediaPartner['url'])) { ?>
                        <a href="<?php echo $mediaPartner['url'];?>" target="_blank" <?php if ($mediaPartner['title']) { ?> title="<?php echo $mediaPartner['title'];?>" <?php } ?>>
                            <img alt="<?php echo $mediaPartner['title'];?>" src="<?php echo $mediaPartner['image'];?>">
                        </a>
                    <?php } else { ?>
                        <img <?php if ($mediaPartner['title']) { ?> alt="<?php echo $mediaPartner['title'];?>" <?php } ?> src="<?php echo $mediaPartner['image'];?>">
                    <?php } ?>
                </li>
            <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>
<?php } ?>