<?php 
global $post;
$next_post = get_next_post();
?>
<article class="post-article" id="post-<?php echo the_ID(); ?>">
	<div class="cnt-wrap">
		<header class="post-header">
			<?php if(!empty( $next_post )) { ?>
			<a class="rfloat link-read-next" href="<?php echo get_permalink( $next_post->ID ); ?>">
			Read next >
			</a>
			<?php } ?>
			<h1 class="post-title">
				<?php the_title();?>
			</h1>
			<h3 class="post-header-meta">
				by <?php 
				$firstname   = get_user_meta($post->post_author, 'first_name', true);
				$lastname    = get_user_meta($post->post_author, 'last_name', true);
				$designation = get_user_meta($post->post_author, 'designation', true);
				$companyName = get_user_meta($post->post_author, 'company_name', true);
				$post_author = $firstname . ' ' . $lastname;
				if (!empty($designation)) {
					$post_author .= ', ' . $designation;
				}
				if (!empty($companyName)) {
					$post_author .= ', '  . $companyName;
				}
				echo $post_author;
				?>
			</h3>
		</header><!-- .entry-header -->
		<div class="post-body">
			<?=get_template_part('sections/wgt-share')?>

			<?php the_content();?>
			
			<div class="align-center">
			<br/>
			<?=get_template_part('sections/wgt-share')?>
			</div>

		</div>

		<div class="post-footer">
			<?php
			$excludeIds = [$post->ID];
			if (!empty( $next_post )): 
				array_push($excludeIds, $next_post->ID);
			?>
			<div class="older-articles">
				<h2 class="txt-yellow"><u>Read Next</u></h2>
				<a href="<?php echo get_permalink( $next_post->ID ); ?>" title="<?php echo $next_post->post_title; ?>" class="item-article"><?php echo $next_post->post_title; ?></a>
			</div>
			<br/>
			<?php endif; ?>
			<?php 
				$related_posts = get_related_post($excludeIds);
				if ($related_posts) {
					$related_posts = $related_posts[0];
			?>
				<div class="related-articles">
					<h2 class="txt-yellow"><u>Related</u></h2>
					<a href="<?php echo $related_posts['link']; ?>" class="item-article" title="<?php echo $related_posts['title_attr']; ?>"><?php echo $related_posts['title']; ?></a>
				</div>
			<?php
				}
			?>
		</div>
	</div>
</article>