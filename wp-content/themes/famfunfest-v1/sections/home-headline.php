<div class="sec-block sec-home-headline nopad">
    <div class="cnt-wrap pure-g">
        <div class="pure-u-1 decoration">
            <img src="/wp-content/themes/babybash-v1/assets/images/decoration.png"/>
        </div>
        <div class="pure-u-1 pure-u-md-1-1 r-box sec-block-padding side-gutter">
            <h2 class="sec-heading txt-red">
                theAsianparent Indonesia - 6th Baby Bash <br />
                EXPLORE CREATIVITY
            </h2>
            <p class="txt-home-headline">
                Baby Bash adalah event playdate paling seru untuk bayi dan keluarga. Di sini, bayi dan orangtua bisa mengikuti berbagai acara seru. Yuk,  segera beli tiketnya sebelum kehabisan!
            </p>
        </div>
    </div>
</div>
<div class="sec-block sec-what-to-expect" id="what-to-expect">
    <div class="cnt-wrap side-gutter">
        <h2 class="sec-heading the-heading">
            DETAIL ACARA
        </h2>
        <p>
            Tanggal : 6 Mei 2018 <br/>
            Waktu   : Pukul 09.00 WIB - 14.00 WIB <br/>
            Tempat : Hotel Grandhika Iskandarsyah
        </p>
    </div>
</div>