<div class="sec-block sec-venue" id="venue">
    <div class="cnt-wrap">
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-2-3">
                <div class="venue-map" tabindex="10">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.1461812638804!2d106.80192321476925!3d-6.244458495479962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f16f12e17173%3A0x9e43454212dc0bd9!2sHotel+GranDhika+Iskandarsyah!5e0!3m2!1sen!2sin!4v1524638647780" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="pure-u-1 pure-u-md-1-3 txt-pink-dark r-box side-gutter">
                <p>
                    <strong>Tempat</strong><br/>
                    Hotel Grandhika Iskandarsyah
                </p>
            </div>
        </div>
        <br/>
    </div>
</div>