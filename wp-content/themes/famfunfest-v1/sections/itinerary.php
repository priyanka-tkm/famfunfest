<?php 
    /*$eventArgs = [
        'numberposts' => -1,
        'order'=> 'ASC',
        'showpastevents' => true,
        'tax_query' => [
            [
                'taxonomy' => 'event-category',
                'operator' => 'IN',
                'field'=>'slug',
                'terms' => ['babybash']
            ]
        ]
    ];*/
    $eventArgs = [
        'numberposts' => -1,
        'showpastevents' => true,
        'order' => 'ASC',
        'orderby' => 'date'
    ];
    $events = eo_get_events($eventArgs);  
?>
<div class="pure-g">
<?php 
    if (count($events) > 0) {
        $count = 1;
        $out = array_chunk($events, ceil(count($events)/$count));
        foreach ($out as $row) : ?>
            <div class="pure-u-1 pure-u-md-1-2 programme-time side-gutter">
                <?php foreach ($row as $event) : 
                    $metadetails      = getAllPostMeta($event->ID); 
                    $eventType        = isset($metadetails['event_type']) ? $metadetails['event_type'] : '';
                    $primaryTagLine   = isset($metadetails['primary_tag_line']) ? $metadetails['primary_tag_line'] : '';
                    $secondaryTagLine = isset($metadetails['secondary_tag_line']) ? $metadetails['secondary_tag_line'] : '';
                    $time             = isset($event->StartTime) ? date("h:i", strtotime($event->StartTime)) : date("h:i", strtotime(date("h:i")));
                    $eventClass = '';
                    if ($eventType == 'null' && empty($primaryTagLine)) {
                        $eventClass = 'iti-break';
                    }
                ?>
                    <dl class="list-itinerary">
                        <dt><?php echo $time;?> WIB</dt>
                        <dd>
                            <?php  
                                $tollTipCls = '';
                                $content = '';
                                if (!empty($event->post_content)) {
                                    $tollTipCls = 'tooltip';
                                    $content = htmlentities(wpautop($event->post_content), ENT_QUOTES);
                                }
                            ?>
                            <div class="iti-name <?php echo $eventClass;?> <?php echo $tollTipCls;?>" data-eventinfo="<?php echo $content;?>">
                                <?php echo $event->post_title;?>
                            </div> 
                        </dd>  
                    </dl>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    <?php } ?>
    <!-- <div class="pure-u-1 pure-u-md-1-2 programme-highlight">
        <h2 class="sec-heading the-heading">
            Highlights featuring:
        </h2>
        <ul>
            <li>10 Voucher Bleaching di Tiga Dental Senilai <strong>Rp. 500.000</strong></li>
            <li>10 Voucher Belanja di Blibli.com Senilai <strong>Rp.100.000</strong></li>
        </ul>
    </div> -->
</div>