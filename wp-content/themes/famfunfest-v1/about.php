<?php
/*
	Template Name: About Page Template
*/

get_header();
?>
<div class="sec-block about-container" id="about-container">
    <div class="cnt-wrap">
		<div class="pure-g">
			<div align="center" class="pure-u-1">
				<img src="/wp-content/themes/babybash-v1/assets/images/about-center1.png">

				<p class="pure-u-1 pure-u-md-1-2 about-text">
					Jadilah bagian dari event playdate paling dinantikan oleh orangtua di Jabodetabek! Banyak ilmu, banyak cerita seru, dan Anda akan pulang dengan goodie bag senilai total jutaan Rupiah!
				</p>
				<p class="pure-u-md-1-2 about-text">
					Tidak percaya? Lihat sendiri keseruan Baby Bash terdahulu lewat tagar #BabyBashID di Instagram atau lewat artikel berikut ini;
					<br />	
					<a href="https://id.theasianparent.com/baby-bash-pertama-di-2018" target="_blank"/>Baby bash pertama 2018</a>
				</p>
				<br />
			</div>
		</div>
	</div>
</div>
<?php
	get_footer();
?>