<?php
class NavigatorsHeader_nav extends Walker_Nav_Menu
{
        private $temp_category = array();
        private $curItem;

        // add main/sub classes to li's and links
        function start_el( &$output, $item, $depth=0, $args=array(), $id=0 ) {
            global $wp_query;
            if($depth == 0){
                 $this->curItem = $item->title;
            }
            $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
            $this->temp_category[] = $item->object_id;
      			$class_names = $value = '';
      			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
      			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
      			$class_names = ' class="pure-menu-item '. esc_attr( $class_names ) . '"';

            // build html
            $output .= "\n" . $indent . '<li id="nav-menu-item-'. $item->object_id . '" '.$class_names.'>';

            // link attributes
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            if (! empty( $item->description)) {
                $attributes .= ! empty( $item->description) ? ' href="'   . esc_attr( $item->description) .'"' : '';
            } else {
                $attributes .= ! empty( $item->url) ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
            }
            $attributes .= ' class="pure-menu-link ' . ( $depth > 0 ? 'pure-sub-menu-link' : 'pure-menu-link' ) . '"';
                $attributes .= ( $depth > 0 ? '' : '' );

            $template = $depth==0 ? '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s' : '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s';

            $item_output = sprintf( $template,
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
            );

            // build html
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

                // build html
            $output .= $indent . '';
        }
}