(function ($, tmpl) {
    var $d = $(document),
        $b = $('body'),
        $hb = $('html, body'),
        _headerHeight = 70,
        _drawerOpen = false,
        $hamburger = null,
        $drawer = null;

    function init () {
        _headerHeight = getHeaderHeight();
        $hamburger = $('#hamburger');
        $drawer = $('#headerNav');
        bindEvents();
        showSpeakerOverlay();
        displayVideo();
    }

    function bindEvents () {
        enableNavScroll();
        enableHamburger();
        enableEventContentPopup();
        enableContactForm();
        enableSponsorForm();
    }
    function displayVideo() {
        $b.on('click', '.video-block .img.is-video:not(.play)', function () {
            var $img = $(this),
                $videoiframe = $img.children('.videoplayer');
            $videoiframe.attr('src', $videoiframe.data('video-url'))
            $img.addClass('play');
        });
    }
    function enableHamburger () {
        $hamburger.on('click', function (e) {
            e.preventDefault();
            var $btn = $(this);
            if($btn.is('.active')) {
                openNavDrawer(false);
            } else {
                openNavDrawer(true);
            }
        });

        $drawer.on('click', function(e) {
            if(e.target === this) {
                openNavDrawer(false);
            }
        });
    }

    function openNavDrawer (show) {
        if(show === false) {
            _drawerOpen = false;
            $b.removeClass('noscroll');
            $hamburger.removeClass('active');
            $drawer.removeClass('active');
        } else {
            _drawerOpen = true;
            $b.addClass('noscroll');
            $hamburger.addClass('active');
            $drawer.addClass('active');
        }
    }
    
    function enableNavScroll () {
        $b.on('click', '.header-nav .pure-menu-link', function (e) {
            var href = e.target.getAttribute('href').replace(/^\//, '');
            // If has hash
            if(href.indexOf('#') === 0) {
                href = href.replace('#', '');
                var section = document.getElementById(href),
                    y = 0;
                if(section) {
                    if(_drawerOpen === true) {
                        // Only for mobile
                        openNavDrawer(false);
                    }
                    y = section.offsetTop - _headerHeight;
                    $hb.animate({scrollTop: y}, 1000);
                    window.location.hash = href;
                    return false;
                }
            }
            return true;
        });
    }

    function getHeaderHeight () {
        return document.getElementById('header').offsetHeight;
    }
    
    function enableSpeakerPopup () {
        $b.on('click', '#speakers .list-speakers > li', function (e) {
            e.preventDefault();
            var speaker_info = JSON.parse(e.currentTarget.getAttribute('data-info'));
            if(speaker_info) {
                showSpeakerOverlay(true, speaker_info);
            }
        });

        $b.on('click', '.speaker-overlay .btn-close', function () {
            showSpeakerOverlay(false);
        });
    }
    function enableEventContentPopup () {
        $b.on('click', '.list-itinerary .tooltip', function (e) {
            e.preventDefault();
            var eventContent = e.currentTarget.getAttribute('data-eventinfo');
            if(eventContent) {
                showEventOverlay(true, {content: eventContent});
            }
        });

        $b.on('click', '.event-overlay .btn-close', function () {
            showEventOverlay(false);
        });
    }

    function showEventOverlay (show, data) {
        if(show === false) {
            $b.removeClass('noscroll');
            $('#popupEventContentOverlay').remove();
        } else if (data) {
            var popup = tmpl('tplEventContentOverlay', data || {});
            $b.addClass('noscroll').append(popup);
        }
    }

    function showSpeakerOverlay (show, data) {
        if(show === false) {
            $b.removeClass('noscroll');
            $('#popupSpeakerOverlay').remove();
        } else if(data) {
            var popup = tmpl('tplSpeakerOverlay', data || {});
            $b.addClass('noscroll').append(popup);
        }
    }

    function enableContactForm () {
        // If Contact us page
        if(!FormValidator) return false;
        var FORM_OK = false;
        var validator = new FormValidator('form-contactus', [
                {
                    name: 'fullname',
                    display: 'nama panjang',
                    rules: 'required'
                }, {
                    name: 'email',
                    rules: 'valid_email'
                }, {
                    name: 'phone',
                    display: 'nomor handphone yang bisa dihubungi',
                    rules: 'required'
                }, {
                    name: 'message',
                    display: 'pesan',
                    rules: 'required'
                },
            ], function(errors, event) {
                if (errors.length > 0) {
                    // Show the errors
                    showAlertBar(true, errors[0].messages.join('<br/>'), 'error');
                    return false;
                }
                FORM_OK = true;
            });

            validator.setMessage('required', 'Wajib mengisi %s');
            validator.setMessage('valid_email', 'Bidang %s harus berisi alamat email yang valid.');

        // Not required if handling POST from server
        $b.on('submit', '#form-contactus', function () {
            if(!FORM_OK) return false;
            var $form = $('#form-contactus');
            var api = "/api/v1/sponsorContact/";
            var email = $form[0].email.value;
            $.post(api, $form.serialize())
                .fail(function (response) {
                    console.log(response);
                    showAlertBar(true, "Ada masalah dalam pengiriman email. Mohon hubungi kami di babybash@tickledmedia.com");
                })
                .done(function (response) {
                    if(response) {
                        showAlertBar(true, response.message);
                        if(response.status === true) {
                            addMixpanelTracking(email, "Babybash Contact Form", "Formulir Submission Baby Bash");
                            $form[0].reset();
                        }
                    }
                });
            FORM_OK = false;
            return false;
        });
    }

    function enableSponsorForm() {
        // If Contact us page
        if(!FormValidator) return false;
        var FORM_OK = false;
        var validator = new FormValidator('form-sponsor', [
                {
                    name: 'fullname',
                    display: 'nama panjang',
                    rules: 'required'
                }, {
                    name: 'email',
                    rules: 'valid_email'
                }, {
                    name: 'message',
                    display: 'pesan',
                    rules: 'required'
                },
            ], function(errors, event) {
                if (errors.length > 0) {
                    // Show the errors
                    showAlertBar(true, errors[0].messages.join('<br/>'), 'error');
                    return false;
                }
                FORM_OK = true;
            });

            validator.setMessage('required', 'Wajib mengisi %s');
            validator.setMessage('valid_email', 'Bidang %s harus berisi alamat email yang valid.');
        
        // Not required if handling POST from server
        $b.on('submit', '#form-sponsor', function () {
            
            if(!FORM_OK) return false;
            var $form = $('#form-sponsor');
            var api = "/api/v1/sponsorContact/";
            var email = $form[0].email.value;
            
            $.post(api, $form.serialize())
                .fail(function (response) {
                    console.log(response);
                    showAlertBar(true, "Ada masalah dalam pengiriman email. Mohon hubungi kami di babybash@tickledmedia.com");
                })
                .done(function (response) {
                    if(response) {
                        showAlertBar(true, response.message);
                        if(response.status === true) {
                            addMixpanelTracking(email, "Babybash Contact Form", "Formulir Submission Baby Bash");
                            $form[0].reset();
                        }
                    }
                });
            FORM_OK = false;
            return false;
        });
    }

    function showAlertBar (show, message, type) {
        var $alertBar = $('#page-alert');
        if(!$alertBar.length) return false;
        if(show === false) {
            $alertBar.slideUp();
        } else {
            $alertBar.children('.cnt-wrap').html(message);
            $alertBar.slideDown('fast');
        }
    }

    function addMixpanelTracking (email, origin, track) {
        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        if (window.mixpanel) {
            window.mixpanel.identify(email);
            window.mixpanel.people.set_once({
                  "$created": datetime,
                   "$email": email,
                   "origin": origin
            });
            //Sent created profile event to Mixpanel
            window.mixpanel.track(track, {
               "url": window.location.href,
               "medium": "babybash site",
               "email": email
            });
        }
    }

    $d.ready(init);
})(jQuery, tmpl);