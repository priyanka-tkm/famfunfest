<?php
$pageHeaderImage      	= get_field('page_header_image', $teamId);
$pageHeaderText      	= get_field('page_header_text', $teamId);
$pageHeaderSubText      = get_field('page_header_sub_text', $teamId);

$image 		= isset($pageHeaderImage['sizes']['large']) ? $pageHeaderImage['sizes']['large'] : '';
$title 		= isset($pageHeaderText) ? $pageHeaderText : '';
$subTitle 	= isset($pageHeaderSubText) ? $pageHeaderSubText : '';

if (!empty($image)) { 
?>
<div class="sub-header-banner">
	<div class="cnt-wrap" style="background-image: url('<?php echo $image; ?>');">
	    <div class="sub-title">
		    <div class="t-title"><?php echo $title; ?></div>
		    <div class="t-subtitle"><?php echo $subTitle; ?></div>
		</div>
	</div>
</div>
<?php } ?>