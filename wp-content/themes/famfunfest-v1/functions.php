<?php
require_once (ABSPATH . '/utils/ImageUtil.php');
require_once (ABSPATH . '/utils/StringUtil.php');
global $imageUtil, $stringUtil;
$imageUtil  = new ImageUtil();
$stringUtil = new StringUtil();

function mixpanelSDK() {
?>
	<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
	0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
	for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
	mixpanel.init("54605a9dc588acdc8e56b6d035b59538");</script><!-- end Mixpanel -->
	<?php
}

add_action('wp_footer', 'mixpanelSDK');	

//* Adding DNS Prefetching
function ism__prefetch() {
?>
	<meta http-equiv="x-dns-prefetch-control" content="on">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//cdn.mxpnl.com">
	<link rel="dns-prefetch" href="//fastcdn.org">
	<link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
	<link rel="prefetch" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png">
<?php }
add_action('wp_head', 'ism__prefetch', 0);

//Add Google Analytics function calls
function add_ga_headers() { 
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
   ga('create', 'UA-61758732-3', 'auto');
//Google Analytics for Content Group.
<?php    
	
	//Send Author name to GA Content Group 5
	//global $author_display_name;
	if(is_single()){ ?>
	     ga('set', 'contentGroup5', "<?php echo get_the_author_meta('display_name'); ?>");
     <?php } else {
	    //Send GA ContentGroup4 with subcategory names
		//Remove categories that shouldn't be displayed
		global $filtered_categories, $post_id;
        $categories = get_the_category($post_id);

		$filtered_categories = get_filtered_categories($categories);

		if($filtered_categories && (array)$filtered_categories === $filtered_categories){
			foreach($filtered_categories as $subcat){
			    	 ?>
ga('set', 'contentGroup4', "<?php echo $subcat->cat_name; ?>");
	    			<?php	 	
			}
		}
	}
    ?>
</script>
<?php
}

add_action('wp_head', 'add_ga_headers', 0);
//Google AMP 

//Return OptionTree data from the database. This function is needed because multisite seems to be unable to retrieve it with the native functions in OptionTree.
function get_options_tree(){
	$site_options = get_option("option_tree");
	return $site_options;
}


//Add the DFP tag scripts depending on the parameters used
function call_dfp_tag($category, $id){
	echo "<script>googletag.cmd.push(function() { googletag.pubads().setTargeting('".$category."','".$id."');});</script>";
}

add_filter( 'locale', 'set_my_locale' );
function set_my_locale( $lang ) {
    return WPLANG;
}
$currentTemplate = get_template();
// Widgets
require_once( ABSPATH . '/wp-content/themes/' . $currentTemplate . '/extends/Navigators/Footer_nav.php' );
require_once( ABSPATH . '/wp-content/themes/' . $currentTemplate . '/extends/Navigators/Header_nav.php' );


// Load translate
load_theme_textdomain( 'theasianparent', ABSPATH . '/wp-content/themes/' . $currentTemplate . '/languages' );

// actived plugin added by th
$actived_plugin = get_option('active_plugins');

// prevent optiontree disable error
if (!function_exists('ot_get_option') && !in_array('option-tree/ot-loader.php', $actived_plugin)) {
	function ot_get_option($key, $default = false) {
		return false;
	}
}


 /**
 * Display related post
 */
 
function get_related_post($excludeIds = [], $count = 1) {
    if (empty($excludeIds)) {
		return false;
	}
	wp_reset_query();
	$args = array(
			'post__not_in' => $excludeIds,
			'posts_per_page' => $count,
			'orderby' => 'rand',
	        'post_type' => 'post'
		);
	$related_query = new WP_Query($args);
	$returned = $related_query->found_posts;
	$related = false;
	if ($related_query->have_posts()) {
		while ($related_query->have_posts()){
			$related_query->the_post();
			$related[] = array(
				'link' => get_permalink(),
				'title' => ucfirst(get_the_title()),
				'title_attr' => strip_tags(get_the_title())
			);
		}
	}
	return $related;
}


/**
 * Display related post (THIS MIGHT NOT BE USED ANYMORE)
 */
function get_related_post_api($post_id, $count) {
	$related = get_related_post($post_id, $count = 1);
	return $related;
}




/** 
 * Check post is in parent category or not
 */
if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
	function post_is_in_descendant_category( $cats, $_post = null ) {
		foreach ( (array) $cats as $cat ) {
			// get_term_children() accepts integer ID only
			$descendants = get_term_children( (int) $cat, 'category' );
			if ( $descendants && in_category( $descendants, $_post ) )
				return true;
		}
		return false;
	}
}


/** 
 * Add page number to article
 */
if ( ! function_exists( 'tap_add_page_number' ) )
{
    function tap_add_page_number( $s )
    {
        global $page;
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
        ! empty ( $page ) && 1 < $page && $paged = $page;

        $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

        return $s;
    }

    add_filter( 'wp_title', 'tap_add_page_number', 100, 1 );
    add_filter( 'wpseo_metadesc', 'tap_add_page_number', 100, 1 );
}

/* Remove Comments url */
add_filter('comment_form_default_fields', 'tap_remove_url');

function tap_remove_url($arg) {
    $arg['url'] = '';
    return $arg;
}
add_action( 'admin_print_styles', 'hide_sticky_option' );
/* Hiding sticky post option */
function hide_sticky_option() {
    global $post_type, $pagenow;
    if( 'post.php' != $pagenow && 'post-new.php' != $pagenow )
        return;
    ?>
    <style type="text/css">#sticky-span { display:none!important }</style>
<?php
}
/*
 * Function similar to wp_link_pages but outputs an ordered list instead and adds a class of current to the current page
 */
 
function tap_link_pages( $args = '' ) {
	$defaults = array(
		'before'           => '<p>' . __( 'Pages:' ), 'after' => '</p>',
		'link_before'      => '', 'link_after' => '',
		'next_or_number'   => 'number', 'nextpagelink' => __( 'Next page' ),
		'previouspagelink' => __( 'Previous page' ), 'pagelink' => '%',
		'echo'             => 1
	);
 
	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );
 
	global $page, $numpages, $multipage, $more, $pagenow;
 
	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			$output .= '<ul class="pagination">';
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				if ( ( $i == $page )) {
					$output .= '<li class="active"><a href="#">';
				} else {
					$output .= '<li>';
				}
				if ( ( $i != $page ) || ( ( ! $more ) && ( $page == 1 ) ) ) {
					$output .= _wp_link_page( $i );
				}
				$output .= $link_before . $j . $link_after;
				$output .= '</a>';
			}
			$output .= '</li>';
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $link_before . $previouspagelink . $link_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $link_before . $nextpagelink . $link_after . '</a>';
				}
				$output .= '</ul>';
				$output .= $after;
			}
		}
	}
 
	if ( $echo )
		echo $output;
 
	return $output;
}
function add_image_lazy_class($content) {
   global $post;
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 lazy"$3>';
   $content = preg_replace($pattern, $replacement, $content);
   return $content;
}
function add_lazyload($content) {

    if ( false !== strpos( $content, 'data-src' ) )
		return $content;
	
	//$placeholder_image ="";
	// In case you want to change the placeholder image
	$placeholder_image = "";
	
	$content = preg_replace( '#<img([^>]+?)src=[\'"]?([^\'"\s>]+)[\'"]?([^>]*)>#', sprintf( '<img${1}src="%s" data-src="${2}"${3}>', $placeholder_image ), $content );
	return $content;
}
/* add_filter('the_content', 'add_image_lazy_class');
add_filter('the_content', 'add_lazyload');
*/

//Limit excerpt to X number of characters
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}


//added class for Previous articles and More articles button

add_filter('next_posts_link_attributes', 'sdac_next_posts_link_attributes');
function sdac_next_posts_link_attributes(){
        return 'class="older"';
}
add_filter('previous_posts_link_attributes', 'sdac_previous_posts_link_attributes');
function sdac_previous_posts_link_attributes(){
        return 'class="newer"';
}


function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
 
function my_deregister_styles() {
	wp_deregister_style( 'wp-admin' );
}
//Mobiles detect exclude iPad

function my_wp_is_mobile() {
    if (
        ! empty($_SERVER['HTTP_USER_AGENT'])

        // bail out, if iPad
        && false !== strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')
    ) return false;
    return wp_is_mobile();
} // function my_wp_is_mobile

add_action('rss2_item', 'add_img_rss_node');
function add_img_rss_node() {
	global $post;
	
	$thumbnail = has_post_thumbnail($post->ID) ? wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'loop_thumb') : false;
								$thumbnail = !empty($thumbnail) ? $thumbnail[0] : get_template_directory_uri() .'/images/default2.png';
	echo "<image><url>".$thumbnail."</url></image>";
	
}
add_filter('cron_schedules','tap_cron_definer');  
function tap_cron_definer($schedules){ 
 $schedules['monthly'] = array(      'interval'=> 2592000,      'display'=>  __('Once Every 30 Days')  );  
 return $schedules;
 }
 
function wpb_full_text_for_feeds( $content ) {
	if ( ! is_feed() )
		return $content;
	global $post;
	$content = $post->post_content;
	return $content;
}

add_filter( 'the_content', 'wpb_full_text_for_feeds', -100 );

//[Linkedin]
function linkedin_func( $atts ){
	$a = shortcode_atts( array(
        'url' => 'linkedinurl',
    ), $atts );

     $script = '<script type="IN/MemberProfile" data-id="'. $a['url'].'" data-format="hover" data-related="false"></script>';
    return $script;
	}
add_shortcode( 'linkedin', 'linkedin_func' );



function cat_is_in_menu( $menu = null, $object_id = null ) {

    // get menu object
    $menu_object = wp_get_nav_menu_items( esc_attr( $menu ) );

    // stop if there isn't a menu
    if( ! $menu_object )
        return false;

    // get the object_id field out of the menu object
    $menu_items = wp_list_pluck( $menu_object, 'object_id' );

    // use the current post if object_id is not specified
    if( !$object_id ) {
        return false;
    }

    // test if the specified page is in the menu or not. return true or false.
    return in_array( (int) $object_id, $menu_items );

}
// add hook
add_filter( 'wp_nav_menu_objects', 'tap_wp_nav_menu_objects_sub_menu', 10, 2 );

// filter_hook function to react on sub_menu flag
function tap_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;
    
    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }
    
    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          } 
        }
      }
    }

    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;

      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }
    
    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}

// Add specific CSS class by filter
add_filter( 'body_class', 'tap_class_names' );
function tap_class_names( $classes ) {
     global $post;
	 $classes[] = '';
	 if(is_single() && in_category('gallery',$post->ID)){
    	 // add 'class-name' to the $classes array
		 $classes[] = 'gallery-type';		
	 }
	  return $classes;
}

function exclude_category($query) {	
      if ($query->is_feed) {
	    $preview_catid = get_cat_ID('Preview');
		$query->set('cat', -$preview_catid);
		}
		return $query;
}
add_filter('pre_get_posts', 'exclude_category');

function get_first_cat_details($category){
	
	if($category && (array)$category === $category){
	global $cat_not_display;
	$first_cat_array = array();
		for($i = 0; $i < sizeof($category); ++$i){
			if(!isset($cat_not_display[$category[$i]->cat_name])){
	    		$catname = $category[$i]->cat_name;
				$catlink = get_category_link( $category[$i]->term_id );
				$catid = $category[$i]->term_id;
				$first_cat_array = array($catname,$catlink,$catid);
				break;
			}
			
		
		}
	}

   return $first_cat_array;
}

function get_filtered_categories($cat_array, $filters=NULL){
	
	if($filters === NULL){
		global $cat_not_display;
		$filters = $cat_not_display;
	}
	
	$filtered_array = array();
	if($cat_array && (array)$cat_array === $cat_array){
	    foreach($cat_array as $subcat){
			if(!isset($filters[$subcat->cat_name])){
				 $filtered_array[] = $subcat;
			}
		}
	}
	
	return $filtered_array;

}

function get_article_type($catid){
        /* Check if the post is in featured or in resources */						
		$type = '';
		$features_id = get_cat_ID('features');
		$resources_id = get_cat_ID('resources');
		if( cat_is_ancestor_of($features_id, $catid )){
    			$type = 'features';
				}
		if( cat_is_ancestor_of($resources_id, $catid )){
    			$type = 'resources';
				}  
		return $type;
}
function get_suitable_cat_details($categories,$categoryId){
        $gall_catid = get_cat_ID('gallery');
		$video_catid = get_cat_ID('Video');
		$features_id = get_cat_ID('features');
		$resources_id = get_cat_ID('resources');
		$type = '';
		$article_type = "";
		
		if( cat_is_ancestor_of($features_id, $categoryId )){
			   $article_type = 'features';
			   }
	    if( cat_is_ancestor_of($resources_id, $categoryId )){
			   $article_type = 'resources';
			   }
        foreach( $categories as $category ) {
			if( cat_is_ancestor_of($categoryId, $category->term_id )){
				$category_link = get_category_link( $category->term_id );
				$category_name = esc_html( $category->name );
				$type = $article_type;
			}
    	}
		if($category_link == ""){
			foreach( $categories as $category ) {
				if($gall_catid == $category->term_id ){
				 $category_link = get_category_link( $category->term_id );
				 $category_name = esc_html( $category->name );
				 $type = '';
				}
    		}
		}
		if($category_link == ""){
			foreach( $categories as $category ) {
				if($video_catid == $category->term_id ){
    				$category_link = get_category_link( $category->term_id );
					$category_name = esc_html( $category->name );
					$type = '';
				}
    		}
		}
		return array($category_name,$category_link,$type);
    
}

//Limit character string shown on home page and related posts so it doesn't overshoot the limited length
function limit_char($string,$char){

	//If this is Thailand, don't shorten string, due to different byte length
	if(country == "th" || country == "lk" || country == "vn"){
		return $string;
	
	}

	if(strlen($string) > $char){	
     $truncatedString = substr($string,0,$char);	
	 $string = substr($truncatedString,0,strrpos($string," "))."...";
	 }
     return $string;
}	
/* load more posts */
function loadMore($max,$paged,$nextPage){
	echo '<div id="load-more"> </div>';
	// Queue JS and CSS
 		wp_enqueue_script(
 			'tap-load-posts',
 			get_stylesheet_directory_uri().'/js/load-posts.js?20151221-1',
 			array('jquery'),
 			'1.1',
 			true
 		);  		
 		// Add some parameters for the JS.
 		wp_localize_script(
 			'tap-load-posts',
 			'tap',
 			array(
 				'startPage' => $paged,
 				'maxPages' => $max,
 				'nextLink' => $catLink.'page/'.$nextPage
 			)
 		); 
}

// hook add_rewrite_rules function into rewrite_rules_array
if (function_exists('add_rewrite_rules')){
	add_filter('rewrite_rules_array', 'add_rewrite_rules');
}



function add_query_vars_filter( $vars ){
  $vars[] = "redirect";
  return $vars;
}

add_filter( 'query_vars', 'add_query_vars_filter' );
add_filter( 'the_content', 'wpautop' , 99);


function pre($str) {
	echo '<pre>';
	print_r($str);
	echo '</pre>';
}

function filterInput($var) {//
	$var = trim($var);
	$var = stripslashes($var);
	$var = htmlentities($var, ENT_QUOTES, "UTF-8");
	return $var;
}

function filterPreSave($var) {
	$trans = array('\\' => "*@^"); // backware slash is creating problem for saving
	$var = strtr($var, $trans);
	return $var;
}

function filterOutput($var) {
	$trans = array('*@^' => "\\");
	$var = strtr($var, $trans);
	return $var;
}

if ( ! function_exists( 'chomum_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function chomum_posted_on() {

	// Get the author name; wrap it in a link.
	$byline = sprintf(
		/* translators: %s: post author */
		__( 'by %s', 'chomum' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
	);

	// Finally, let's write all of this to the page.
	echo '<span class="byline"> ' . $byline . '</span>';
}
endif;

if ( ! function_exists( 'chomum_time_link' ) ) :
/**
 * Gets a nicely formatted string for the published date.
 */
function chomum_time_link() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		get_the_date( DATE_W3C ),
		get_the_date(),
		get_the_modified_date( DATE_W3C ),
		get_the_modified_date()
	);

	// Wrap the time string in a link, and preface it with 'Posted on'.
	return sprintf(
		/* translators: %s: post date */
		__( '<span class="screen-reader-text">Posted on</span> %s', 'chomum' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);
}
endif;


if ( ! function_exists( 'chomum_edit_link' ) ) :
/**
 * Returns an accessibility-friendly link to edit a post or page.
 *
 * This also gives us a little context about what exactly we're editing
 * (post or page?) so that users understand a bit more where they are in terms
 * of the template hierarchy and their content. Helpful when/if the single-page
 * layout with multiple posts/pages shown gets confusing.
 */
function chomum_edit_link() {

	$link = edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'chomum' ),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);

	return $link;
}
endif;

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function chomum_setup() {

	load_theme_textdomain( 'chomum' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'post-thumbnails' );

	register_nav_menus(array(
		'navigation_menu_primary' => 'Header menu',
		'footer_menu' => 'Footer links'
	));

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'chomum_setup' );

/** GET POST META DETAILS **/
function getAllPostMeta($post_id) {
	global $wpdb;
	$results = array();
	$wpdb->query("
	    SELECT `meta_key`, `meta_value`
	    FROM $wpdb->postmeta
	    WHERE `post_id` = $post_id
	    ");
	foreach ($wpdb->last_result as $key => $value) {
	    $results[$value->meta_key] = $value->meta_value;
	}
	return $results;
}


function createPostTypeBrand() {
	$labels = array(
	'name' => __('Brands', 'theasianparent'),
	'singular_name' => __('Brand', 'theasianparent'),
	'add_new' => __('Add New', 'theasianparent'),
	'add_new_item' => __('Add New Brand', 'theasianparent'),
	'edit_item' => __('Edit Brand', 'theasianparent'),
	'new_item' => __('New Brand', 'theasianparent'),
	'view_item' => __('View Brand', 'theasianparent'),
	'search_items' => __('Search Brand', 'theasianparent'),
	'not_found' => __('No Brand found', 'theasianparent'),
	'not_found_in_trash' => __('No Brand found in Trash', 'theasianparent'),
	'parent_item_colon' => ''
	);

	$args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => false,
	'menu_position' => 21,
	'supports' => array('title', 'editor', 'author', 'thumbnail'),
	'taxonomies' => array('brands') // this is IMPORTANT
	);
	register_post_type('brand',$args);
}

function brandInit() {
	$labels = array(
	'name' => __( 'Categories', 'theasianparent'),
	'singular_name' => __( 'Categories', 'theasianparent')
	);
	register_taxonomy('brands','brand', array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'brand'),
	));
}

function createPostTypeGoodie() {
	$labels = array(
	'name' => __('Goodies', 'theasianparent'),
	'singular_name' => __('Goodie', 'theasianparent'),
	'add_new' => __('Add New', 'theasianparent'),
	'add_new_item' => __('Add New Goodie', 'theasianparent'),
	'edit_item' => __('Edit Goodie', 'theasianparent'),
	'new_item' => __('New Goodie', 'theasianparent'),
	'view_item' => __('View Goodie', 'theasianparent'),
	'search_items' => __('Search Goodie', 'theasianparent'),
	'not_found' => __('No Goodie found', 'theasianparent'),
	'not_found_in_trash' => __('No Goodie found in Trash', 'theasianparent'),
	'parent_item_colon' => ''
	);

	$args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => false,
	'menu_position' => 21,
	'supports' => array('title', 'editor', 'author', 'thumbnail'),
	'taxonomies' => array('goodies') // this is IMPORTANT
	);
	register_post_type('goodie',$args);
}

function goodieInit() {
	$labels = array(
	'name' => __( 'Categories', 'theasianparent'),
	'singular_name' => __( 'Categories', 'theasianparent')
	);
	register_taxonomy('goodies','goodie', array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'goodie'),
	));
}

function createPostTypeLuckyDraw() {
	$labels = array(
	'name' => __('Lucky Draw', 'theasianparent'),
	'singular_name' => __('Lucky Draw', 'theasianparent'),
	'add_new' => __('Add New', 'theasianparent'),
	'add_new_item' => __('Add New Lucky Draw', 'theasianparent'),
	'edit_item' => __('Edit Lucky Draw', 'theasianparent'),
	'new_item' => __('New Lucky Draw', 'theasianparent'),
	'view_item' => __('View Lucky Draw', 'theasianparent'),
	'search_items' => __('Search Lucky Draw', 'theasianparent'),
	'not_found' => __('No Lucky Draw found', 'theasianparent'),
	'not_found_in_trash' => __('No Lucky Draw found in Trash', 'theasianparent'),
	'parent_item_colon' => ''
	);

	$args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => false,
	'menu_position' => 21,
	'supports' => array('title', 'editor', 'author', 'thumbnail'),
	'taxonomies' => array('luckydraw') // this is IMPORTANT
	);
	register_post_type('luckydraw',$args);
}

function luckydrawInit() {
	$labels = array(
	'name' => __( 'Categories', 'theasianparent'),
	'singular_name' => __( 'Categories', 'theasianparent')
	);
	register_taxonomy('luckydraws','luckydraw', array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'luckydraw'),
	));
}

if (is_admin()) {
	add_action( 'init', 'createPostTypeBrand');
	add_action( 'init', 'brandInit');

	add_action( 'init', 'createPostTypeGoodie');
	add_action( 'init', 'goodieInit');

	add_action( 'init', 'createPostTypeLuckyDraw');
	add_action( 'init', 'luckydrawInit');
}