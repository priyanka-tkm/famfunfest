<footer class="footer align-center">
    <div class="cnt-wrap">
    	<ul class="footer-nav">
        <?php
		/**
		 * Display footer link
		 */
		wp_nav_menu( array(
			'theme_location'    => 'footer_menu',
		    'menu'              => 'Footer links',
		    'container'         => '',
		    'container_class'   => 'row',
		    'menu_class'        => 'row', 
		    'echo'              => true,
		    'items_wrap'        => '%3$s',
		    'depth'             => 10,
		) ); 
		?>
		</ul>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/app.js?ver=20170507"></script>
</body>
</html>