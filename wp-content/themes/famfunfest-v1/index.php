<?php 
get_header();
global $post;
if(is_single() && $post->post_type == 'post') {
    get_template_part( 'sections/post/content', '' );
} else {
	get_template_part( 'sections/page/home', 'page' );
}
get_footer(); 
?>
