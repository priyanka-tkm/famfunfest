<?php
/**
 * TAP CHOmum Conference site
 * @author Elton Jain http://github.com/scazzy
 */
require_once (ABSPATH . '/utils/StringUtil.php');
global $stringUtil;
$stringUtil = new StringUtil();
$fbappid = 1352433634788083;
?>
<!DOCTYPE html>
<html xmlns:fb="https://www.facebook.com/2008/fbml" prefix="og: http://ogp.me/ns#" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/app.css?v1=20180227-1"/>  
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.png" />
    <meta name="author" content="BabyBash" />
    <meta property="fb:app_id" content="<?php echo $fbappid;?>" /> 
<?php
if (is_single()) { /* if page is content page */ ?>
    <meta itemprop="name" content="<?php single_post_title(''); ?>">
    <?php 
    global $post;
    $postCustom = get_post_custom($post->ID);
    $yoastMetaDescription = !empty($postCustom['_yoast_wpseo_metadesc'][0]) ? $postCustom['_yoast_wpseo_metadesc'][0] : $stringUtil->truncateContentString($post->post_content, 350, ''); 
    $author_id = $post->post_author;
    $author_fb_url = get_the_author_meta( 'facebook' , $author_id );
    if ($author_fb_url=="") {
      $author_fb_url = "https://www.facebook.com/theBabyBash";
    }
    ?>
    <meta itemprop="description" content="<?php echo $yoastMetaDescription; ?>">
    <meta itemprop="image" content="<?php if (function_exists('wp_get_attachment_thumb_url')) { echo wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID)); } ?>">
    <meta property="article:author" content="<?php echo $author_fb_url; ?>" />
<?php } ?>
<?php wp_head(); ?>
<?php 
//require "google-dfp.php"; 
?>
<script>
ga('send', {
  hitType: 'pageview',
  page: location.pathname,
  title: document.title
  });
</script>  
</head>
<body <?php body_class(); ?>>
<header class="header" id="header">
    <div class="cnt-wrap pure-g">
        <h1 class="pure-u-1 pure-u-md-1-5 logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="BabyBash"/>
            </a>
        </h1>
        <button class="hamburger" id="hamburger">
            <span class="menu-inner"></span>
        </button>
        <div class="pure-u-1 pure-u-md-4-5 align-right right">
            <nav class="pure-menu pure-menu-horizontal pure-menu-scrollable header-nav" id="headerNav">
            <?php 
                $args = array(
                'theme_location' => 'navigation_menu_primary',
                'menu' => 'Top menu bar',
                'depth'      => 0,
                'container'  => false,
                'items_wrap'      => '<ul id="%1$s" class="pure-menu-list">%3$s</ul>',
                'walker'     => new NavigatorsHeader_nav()
                );
                wp_nav_menu($args);
            ?>
            </nav>
        </div>
    </div>
</header>
<div class="header-placeholder"></div>
<?php 
get_template_part( 'subHeader', '');
?>
