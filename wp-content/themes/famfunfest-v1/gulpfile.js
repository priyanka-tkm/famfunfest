var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('css', function(){
  return gulp.src('assets/source/less/app.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css'))
});

gulp.task('js', function() {
  gulp.src(['assets/source/js/lib/*.js', 'assets/source/js/app.js'])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js/'))
});

gulp.task('watch', function () {
    gulp.watch('assets/source/less/*.less', ['css']);
    gulp.watch('assets/source/js/**/*.js', ['js']);
});

// Default Task
gulp.task('default', ['watch']);