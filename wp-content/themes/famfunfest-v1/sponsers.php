<?php
/*
	Template Name: Sponsers Page Template
*/

get_header();

get_template_part('sections/sponsors');

get_template_part('sections/alertbar');
?>

<div class="sec-block sec-sponsors sec-sponsors-form side-gutter" id="sponsor">
    <div class="cnt-wrap">
        <h2 class="sec-heading the-heading align-center">
            Ingin mensponsori<br />
			Baby Bash?
        </h2>
        <br/>
        <form method="post" id="form-sponsor" class="pure-form form-full-width pure-g" name="form-sponsor">
            <div class="pure-u-1 field">
                <input type="text" placeholder="Nama" name="fullname" />
            </div>
            <div class="pure-u-1 field">
                <input type="email" placeholder="E-mail" name="email"  />
            </div>
            <div class="pure-u-1 field">
                <textarea placeholder="Tulis pesan Anda" rows="5" name="message" ></textarea>
            </div>
            <div class="pure-u-1 align-center field">
                 <input type="hidden" name="action" value="sponsor" />
                <button class="pure-button button-blue-dark button-send">Kirim</button>
            </div>
        </form>
    </div>
</div>

<?php
get_footer();
?>