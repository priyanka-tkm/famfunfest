<?php
/*
	Template Name: Programme Page Template
*/

get_header();
?>
<div class="sec-block sec-prommee">
    <div class="cnt-wrap">
		<?php get_template_part('sections/itinerary'); ?>
	        <div class="lucky-draw-banner" id="luckydraw">
				<div class="cnt-wrap" style="background-image: url('/wp-content/themes/babybash-v1/assets/images/luckydraw.jpg');">
				    <div class="sub-title">
					    <div class="t-title">Lucky Draw</div>
					    <div class="t-subtitle">Menangkan hadiah-hadiah di bawah ini!</div>
					</div>
				</div>
			</div>
			
			<?php //get_template_part('sections/brands'); ?>
			<?php get_template_part('sections/lucky-draw'); ?>
			
			<div class="lucky-draw-banner" id="goodiebag">
				<div class="cnt-wrap" style="background-image: url('/wp-content/themes/babybash-v1/assets/images/goodiebag.jpg');">

				    <div class="sub-title">
					    <div class="t-title">Goodie Bag</div>
					    <div class="t-subtitle">Ada banyak gifts yang bisa Parents bawa pulang dari sponsor kami:</div>
					</div>
				</div>
			</div>

			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-1 amazing-prize side-gutter">
					<!--<h2 class="sec-heading">Banyak hadiah menarik yang bisa dibawa pulang!</h2>-->
				</div>
			</div>

			<?php
				$args = [
		        	'numberposts' =>-1,
		        	'post_type' => 'goodie',
		        	'orderby'	=> 'date',
		        	'order'		=> 'asc'
		      	];
	      		$goodie = get_posts($args);

	      		if (count($goodie) > 0) {
					$count = 2;
        			$goodieObj = array_chunk($goodie, ceil(count($goodie)/$count));
			?>

			<div class="pure-g">
				<div class="pure-u-0 pure-u-sm-1-6">&nbsp;</div>
				<?php foreach ($goodieObj as $row) : ?>
					<div class="pure-u-1 pure-u-sm-1-3 goodies side-gutter">
		                <?php foreach ($row as $goodie) : ?>
		                <ul>
		                    <li><?php echo $goodie->post_title;?></li>
		                 </ul>
		                <?php endforeach; ?>
		            </div>
				<?php endforeach; ?>
			</div>
		<?php } ?>
			<p class="align-center"><strong>Dan masih banyak lagi!</strong></p>
			<br/>
	</div>
</div>

<?php
	get_footer();
?>